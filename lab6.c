#include <stdio.h>
void inputarr(int *a,int n)
{
    int i;
    printf("Enter %d elements of which avg you want: ",n);
    for(i=0;i<n;i++)
    {
        scanf("%d",&a[i]);
    }
}
float calculate(int *a ,int n,float avg)
{
    int i;
    float sum=0;
    for(i=0;i<n;i++)
    {
        sum=sum+a[i];
    }
    avg=sum/n;
    return avg;

}
void display(float avg)
{

    printf("Average of given input is :%f ",avg);
}
int main()
{
    int n;
    int a[1000];
    float avg;
    printf("Enter the number of elements: ");
    scanf("%d",&n);
    inputarr(a,n);
    avg=calculate(a,n,avg);
    display(avg);

    return 0;
}
