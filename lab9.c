#include <stdio.h>

struct student
{
    int roll_no,tmarks;
    char name[25],sec,dep[3];
    float fees;
};

int main()
{
    int i,j,n;
    struct student stud[100],t;
    printf("Enter the no of students\n");
    scanf("%d",&n);

    for(i=0;i<n;i++)
    {
        printf("enter %d student info of as roll_no , name , tmarks,sec,dep[initals],fees\n",i+1);
        scanf("%d %s %d %c %s %f",&stud[i].roll_no,stud[i].name,&stud[i].tmarks,&stud[i].sec,stud[i].dep,&stud[i].fees);
    }

    for(i=0;i<n;i++)
    {
        for(j=0;j<n-1;j++)
        {
            if(stud[j].tmarks<stud[j+1].tmarks)
            {
                t=stud[j];
                stud[j]=stud[j+1];
                stud[j+1]=t;
            }
        }
    }

    printf("\n Student who got the highest marks is\n");
    printf("\n roll_no: %d \n name: %s \n tmarks: %d \n Section: %c \n department: %s \n fees: %f \n",stud[0].roll_no,stud[0].name,stud[0].tmarks,stud[0].sec,stud[0].dep,stud[0].fees);


    return 0;
}