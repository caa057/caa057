include <stdio.h>
#include <math.h>
int main()
{
    int a,b,c;
    float D,root1,root2;
    printf("Enter the coefficients of all terms of the quadratic equation \n");
    scanf("%d %d %d",&a,&b,&c);
    D=(b*b)-(4*a*c);
    if(D==0)
    {
        printf("Roots are real");
        root1=(-b+D)/(2*a);
        root2=(-b-D)/(2*a);
        printf("Roots are %f and%f",root1,root2);
    }
    else if(D>0)
    {
        printf("Roots are not equal");
        root1=(-b+sqrt(D))/(2*a);
        root2=(-b-sqrt(D))/(2*a);
        printf("Roots are %f and %f",root1,root2);
    }
    else if(D<0)
    {
        printf("Roots are imaginary");
    }
    return 0;
 }
  