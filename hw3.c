#include <stdio.h>

int main()
{
    int a[10], b[10],c[10],n;
    int i,square,cube;

    printf("enter number of elements ");
    scanf("%d", &n);
    printf("enter the  elements\n");
    for (i = 0; i < n; i++) 
    {
        printf("a[%d]=",i);
        scanf("%d", &a[i]);
    }
    printf("\nthe square array is :\n");
    for(i=0;i<n;i++)
    {
        square=a[i]*a[i];

        printf("\nb[%d]=%d",i,square);
    }
    printf("\nthe cube array is :\n");
    for(i=0;i<n;i++)
    {
        cube=a[i]*a[i]*a[i];

        printf("\nc[%d]=%d",i,cube);
    }

    return 0;
}