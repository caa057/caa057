#include <stdio.h>
int sum(int n)
{
    int p,sum=0,remainder;
    p = n;

    while (p != 0)
    {
        remainder = p % 10;
        sum       = sum + remainder;
        p         = p/ 10;
    }
    printf("Sum of digits of %d = %d\n", n, sum);
    return sum;
}
int main()
{
    int a;

    printf("Enter an integer\n");
    scanf("%d", &a);
    sum(a);
    return 0;
}