#include <stdio.h>

int main()
{
    int a,b,c,*x,*y;
    printf("enter the two numbers :\n");
    scanf("%d%d",&a,&b);
    x=&a;
    y=&b;
    c=*x;
    *x=*y;
    *y=c;
    printf("the new value of the numbers is\n");
    printf("a=%d & b=%d",a,b);
    return 0;
}
