#include <stdio.h>

int gcd_v (int x, int y)
{
    int gcd;
    for (int i = 1; i <= x && i <= y; ++i)
    {
        if (x % i == 0 && y % i == 0)
            gcd = i;
    }
    return gcd;
}


int main ()
{
    int gcd, x, y;
    printf ("Enter the numbers whose gcd you want ? ");
    scanf ("%d%d", &x, &y);
    gcd = gcd_v (x, y);
    printf ("The Gcd of %d and %d is %d", x, y, gcd);

    return 0;
}